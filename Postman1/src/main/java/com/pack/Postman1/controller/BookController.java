package com.pack.Postman1.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pack.Postman1.Postman1Application;
import com.pack.Postman1.dao.BookRepository;
import com.pack.Postman1.model.BookStore;

@RestController
public class BookController {
	
	@Autowired 
	BookRepository repository;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(Postman1Application.class);
	@PostMapping(value="/book")
	public ResponseEntity<BookStore> saveBook(@RequestBody BookStore bookStore)   
	{  
		try {
			LOGGER.info("books created");
			BookStore _store=repository.save(new BookStore(bookStore.getBookId(),bookStore.getBookName(),bookStore.getBookAuthor(),bookStore.getPrice()));
			return new ResponseEntity<>(_store,HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(null,HttpStatus.EXPECTATION_FAILED);
		}
	}
	@GetMapping(value="/books")
	public ResponseEntity<List<BookStore>> getAllBooks()
	{
		List<BookStore> bookStore=new ArrayList<BookStore>();
		try
		{
			LOGGER.info("List of books obtained");
			repository.findAll().forEach(bookStore::add);
			if(bookStore.isEmpty())
			{
				return new ResponseEntity<>(bookStore,HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(bookStore,HttpStatus.OK);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping(value="/books/bookName/{bookName}")
	public ResponseEntity<List<BookStore>> findBybookName(@PathVariable String bookName)
	{
		try
		{
			LOGGER.info("searching by bookName");
			List<BookStore> books=repository.findBybookName(bookName);
			if(books.isEmpty())
			{
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(HttpStatus.OK);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<HttpStatus> deleteBook(@PathVariable("bookId") int bookId)
	{
		try
		{
			repository.deleteById(bookId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	
	
}
