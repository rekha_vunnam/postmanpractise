package com.pack.Postman1.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="books")
public class BookStore {
	@Id
	private int bookId;
	private String bookName;
	private String bookAuthor;
	private double price;
	public BookStore() {
		super();
		
	}
	public BookStore(int bookId, String bookName, String bookAuthor, double price) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.price = price;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getBookAuthor() {
		return bookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "BookStore [bookId=" + bookId + ", bookName=" + bookName + ", bookAuthor=" + bookAuthor + ", price="
				+ price + "]";
	}
	
	

}
