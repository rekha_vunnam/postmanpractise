package com.pack.Postman1.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.pack.Postman1.model.BookStore;

public interface BookRepository extends CrudRepository<BookStore,Integer>{

	List<BookStore> findBybookName(String bookName);

}
